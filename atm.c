#include <stdio.h>
#include <stdlib.h>

#define HIDROGENIO 1
#define HELIO 5
#define GRAVIDADE 10
#define GAS 50

int main(int argc, char *argv[]) {
	int value;
	int h, he, gra, gas;
	
	if(argc > 1) {
		value = atoi(argv[1]);
	} else {
		printf("use: atm <value>\n");
		return 1;
	}		
			
	printf("Valor %d\n\n", value);
	
	gas = value/GAS;
	value = value % GAS;
	
	gra = value/GRAVIDADE;
	value = value % GRAVIDADE;
	
	he = value/HELIO;
	value = value % HELIO;
	
	h = value;
	
	printf("Hidrogenio %d\n", h);
	printf("Helio %d\n", he);
	printf("Gravidade %d\n", gra);
	printf("Gas %d\n", gas);
	
	return 0;
}
